import { Component, OnInit } from '@angular/core';
import { BasicauthenticationService } from '../services/basicauthentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {


  logoutCntr = false;
  logoutMessage = 'Çıkış Yaptınız';

  constructor(private router: Router,
              private basicAuthenticationService: BasicauthenticationService) { }



  ngOnInit() {
    this.logout();
  }
  logout() {
    this.basicAuthenticationService.logout().subscribe(
      response => console.log(response),
      err => console.log(err)
    );
    this.logoutCntr = true;
    setTimeout(() => {this.logoutCntr = false; this.router.navigate(['login']); }, 3000);
  }

}
