import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { BasicauthenticationService } from './basicauthentication.service';

@Injectable({
  providedIn: 'root'
})
export class RouteguardService {
 

  constructor(private router: Router, private basicauthenticationService: BasicauthenticationService) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if (this.basicauthenticationService.isUserLoggedIn()) {
      return true;
    }
    // Sayfalara yetkisiz giriş yapılmak istenirse login e yönlendirilecek
    this.router.navigate(['login']);
    return false;

  }


}
