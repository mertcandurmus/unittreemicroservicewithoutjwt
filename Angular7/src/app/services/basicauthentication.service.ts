import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {map, tap, catchError} from 'rxjs/operators';
import { API_URL } from 'src/app.constants';
import { User } from '../models/user';
import { Observable, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';











export const TOKEN = 'token';
export const AUTHENTICATED_USER = 'authenticatedUser';

@Injectable({
  providedIn: 'root'
})
export class BasicauthenticationService {
  token1: any;
  token2;



  constructor(private http: HttpClient,  private cookieService: CookieService) {
   }

   path = 'http://localhost:8080/signup';
   path2 = 'http://localhost:8080/signout';


   loadToken() {
    this.token2 = localStorage.getItem('token1');
   }
// Security Bilgilerini alıyoruz
   executeJWTAuthenticationService(username, password) {
   return this.http.post<any>(
     `${API_URL}/signin`, {
     username,
     password
    }).pipe(
       map(
         data => {
           sessionStorage.setItem(AUTHENTICATED_USER, username);
           this.token1 = `Bearer ${data.accessToken}`;
           window.localStorage.removeItem('token1');
           window.localStorage.setItem('token1', this.token1);
           sessionStorage.setItem(TOKEN, `Bearer ${data.accessToken}`);
           return data;
         }
       )
     );
}

addUser(user: User): Observable<User> {

  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  return this.http.post<User>(this.path, user, httpOptions).pipe(
    tap(data => console.log(JSON.stringify(data))),
    catchError(this.handleError)
  );
}





handleError(err: HttpErrorResponse) {
  let errorMessage = '';
  if (err.error instanceof ErrorEvent) {
    errorMessage = 'bir hata oluştu' + err.error.message;
  } else {
    errorMessage = 'sistemsel bir hata';
  }

  return throwError(errorMessage);
}






// aldığımız username ve password bilgilerini servis tarafına aktarıyoruz
executeAuthenticationService(username, password) {
  // Security Bilgilerini alıyoruz

  const basicAuthHeaderString = 'Basic ' + window.btoa(username + ':' + password);

  // Servisin header alanına security bilgilerimizi gönderiyoruz
  const headers = new HttpHeaders({
    Authorization: basicAuthHeaderString
  });

  return this.http.get<AuthenticationBean>(

   `${API_URL}/api/auth/signin`,
   {headers}).pipe(
     map(
       data => {
         sessionStorage.setItem(AUTHENTICATED_USER, username);
         sessionStorage.setItem(TOKEN, basicAuthHeaderString);
         return data;
       }
     )
   );
}




getAuthenticatedUser() {
  return sessionStorage.getItem(AUTHENTICATED_USER);
}




getAuthenticatedToken() {
  // user boş değilse
  if (this.getAuthenticatedUser()) {
    return sessionStorage.getItem(TOKEN);
  }
}




// session storage da mevcut authenticatedUser varsa getir.
isUserLoggedIn() {
  const user = sessionStorage.getItem(AUTHENTICATED_USER);
  return !(user === null);
}








logout() {
  sessionStorage.removeItem(AUTHENTICATED_USER);
  sessionStorage.removeItem(TOKEN);

  this.cookieService.deleteAll();

  this.loadToken();

  this.deleteAllCookies();



  const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  Authorization: this.token2 })
};
  return this.http.post('http://localhost:8080/signout', '' , httpOptions).pipe(
    tap(data => console.log(JSON.stringify(data))),
    catchError(this.handleError)
  );


}

 deleteAllCookies() {
  const cookies = document.cookie.split(';');

  // tslint:disable-next-line:prefer-for-of
  for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i];
      const eqPos = cookie.indexOf('=');
      const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
  }
}

/*
 onRemoved(cookie) {
  console.log(`Removed: ${cookie}`);
}

 onError(error) {
  console.log(`Error removing cookie: ${error}`);
}

 removeCookie(tabs) {
  const globals = require('protractor');
  const browser: ProtractorBrowser = globals.browser;

  const removing = browser.cookies.remove({
    url: tabs[0].url,
    name: 'JSESSIONID'
  });
  removing.then(this.onRemoved, this.onError);
}

*/

}




export class AuthenticationBean {
  constructor(public message: string) {
  }
}




