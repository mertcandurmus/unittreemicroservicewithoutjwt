import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { Unit } from '../models/unit';
import { Observable, throwError } from 'rxjs';
import { BasicauthenticationService } from './basicauthentication.service';



@Injectable({
  providedIn: 'root'
})
export class UnitService {
  constructor(private http: HttpClient, private basicauthenticationService: BasicauthenticationService) {}

  path = 'http://localhost:8081/unit';
  path2 = 'http://localhost:8081/unit';
  path3 = 'http://localhost:8081/auth/authx';

  getUnits(): Observable<Unit[]> {
    this.basicauthenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
      Authorization: this.basicauthenticationService.token2 })
    };
    return this.http.get<Unit[]>(this.path + '/units', httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getTypes(): Observable<string> {
   // this.basicauthenticationService.loadToken();
   this.basicauthenticationService.loadToken();
   const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
    Authorization: this.basicauthenticationService.token2 })
  };
   return this.http.get<string>(this.path2 + '/types', httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }
  getAuth(): Observable<string> {
    // this.basicauthenticationService.loadToken();
    this.basicauthenticationService.loadToken();
    const httpOptions = {
     headers: new HttpHeaders({ 'Content-Type': 'application/json',
     Authorization: this.basicauthenticationService.token2 })
   };
    return this.http.get<string>(this.path3, httpOptions).pipe(
       tap(data => console.log(JSON.stringify(data))),
       catchError(this.handleError)
     );
   }

  handleError(err: HttpErrorResponse) {

    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = 'bir hata oluştu' + err.error.message;
    } else {
      errorMessage = 'sistemsel bir hata';
    }

    return throwError(errorMessage);
  }



  addUnit(unit: Unit): Observable<Unit> {
 // this.basicauthenticationService.loadToken();
 this.basicauthenticationService.loadToken();
 const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  Authorization: this.basicauthenticationService.token2 })
};
 return this.http.post<Unit>(this.path + '/add', unit, httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }


  updateUnit(unit: Unit): Observable<Unit> {

this.basicauthenticationService.loadToken();
const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
      Authorization: this.basicauthenticationService.token2 })
    };
return this.http.post<Unit>(this.path + '/update', unit, httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getUnitOne(id: string): Observable<Unit> {

 //   this.basicauthenticationService.loadToken();
 this.basicauthenticationService.loadToken();
 const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  Authorization: this.basicauthenticationService.token2 })
};
 return this.http.get<Unit>(`${this.path}/units/${id}`, httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  deleteUnit2(id: number): Observable<Unit[]> {

  this.basicauthenticationService.loadToken();
  const tokencntr = this.basicauthenticationService.getAuthenticatedToken();
  console.log(tokencntr);
  const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
    Authorization: tokencntr })
  };
  return this.http.post<Unit[]>(`${this.path}/delete/${id}`, httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );

  }

  deleteUnit(id: number): Observable<Unit[]> {

    this.basicauthenticationService.loadToken();
    const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  Authorization: this.basicauthenticationService.token2 })
};
    console.log(this.basicauthenticationService.token2 );
    return this.http.post<Unit[]>(`${this.path}/delete/${id}`, httpOptions).pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );

    }


  getLeastUnit(attachedunit: number): Observable<Unit[]> {

    this.basicauthenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
      Authorization: this.basicauthenticationService.token2 })
    };
    return this.http.get<Unit[]>(`${this.path}/unitAttach/${attachedunit}`, httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );

  }

}
