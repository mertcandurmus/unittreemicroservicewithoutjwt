import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BasicauthenticationService } from '../services/basicauthentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  username = '';
  password = '';
  errorMessage = 'Girilen bilgilerin en az birisi hatalı!!!';
  errorMessage2 = 'kullanıcı aktif olarak giriş yapmış durumda!!!';
  invalidLogin = false;
  ifLogin = false;
  token1;

  constructor(private router: Router,
              private basicAuthenticationService: BasicauthenticationService) { }

  ngOnInit() {
    this.ifLogin = this.basicAuthenticationService.isUserLoggedIn();
  }


  handleJWTAuthLogin() {
    this.basicAuthenticationService.executeJWTAuthenticationService(this.username, this.password)
      .subscribe(
        data => {
          // tslint:disable-next-line:max-line-length
          this.router.navigate(['unit']);
          this.invalidLogin = false;
          console.log(this.basicAuthenticationService.getAuthenticatedToken());
          this.token1 = this.basicAuthenticationService.getAuthenticatedToken();
          window.localStorage.removeItem('token1');
          window.localStorage.setItem('token1', this.token1);
          console.log(this.basicAuthenticationService.getAuthenticatedUser());
        },
        error => {
          console.log(error);
          this.invalidLogin = true;
        }
      );
}

register() {
  this.router.navigate(['register']);
}



}
