import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirimGuncelleComponent } from './birim-guncelle.component';

describe('BirimGuncelleComponent', () => {
  let component: BirimGuncelleComponent;
  let fixture: ComponentFixture<BirimGuncelleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirimGuncelleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirimGuncelleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
