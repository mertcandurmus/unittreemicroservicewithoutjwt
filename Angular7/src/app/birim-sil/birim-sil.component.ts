import { Component, OnInit } from '@angular/core';
import { Unit } from '../models/unit';
import { UnitService } from '../services/unit.service';
import { ConfirmDialogService } from '../services/confirm-dialog.service';


@Component({
  selector: 'app-birim-sil',
  templateUrl: './birim-sil.component.html',
  styleUrls: ['./birim-sil.component.css']
})
export class BirimSilComponent implements OnInit {

  constructor(private unitService: UnitService, private confirmDialogService: ConfirmDialogService) { }

  unit: Unit[];
  showMsg;
  ctrl = true;
  ctrl2;


  ngOnInit() {
    this.unitService.getUnits().subscribe(data => {
      this.unit = data;
    });
  }

deleteUnit(id: number) {
 // this.myFunc();
this.unitService.deleteUnit(id).subscribe(
    data => {
      console.log(data);
    //  this.reloadData();
      this.showMsg = true;
      setTimeout(() => {this.showMsg = false; }, 2000);
    },
    error => console.log(error));
  }


  reloadData() {
    this.unitService.getUnits().subscribe(data => {
      this.unit = data;
    });
  }

myFunc() {
  this.ctrl2 = confirm('Emin misiniz');
  if (this.ctrl2 === true) {
    this.ctrl = true;
  } else {
  this.ctrl = false;
}
}



}


// <button class="btn btn-primary pull-right" (click)="showDialog()">Show Dialog</button>
