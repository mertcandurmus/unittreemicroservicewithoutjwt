import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirimSilComponent } from './birim-sil.component';

describe('BirimSilComponent', () => {
  let component: BirimSilComponent;
  let fixture: ComponentFixture<BirimSilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirimSilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirimSilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
