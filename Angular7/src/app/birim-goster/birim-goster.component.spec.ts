import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirimGosterComponent } from './birim-goster.component';

describe('BirimGosterComponent', () => {
  let component: BirimGosterComponent;
  let fixture: ComponentFixture<BirimGosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirimGosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirimGosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
