export class Unit {
    id: number;
    type: string;
    name: string;
    opendate: Date;
    remark: string;
    attachedunit: string;
    typeid: number;
}
