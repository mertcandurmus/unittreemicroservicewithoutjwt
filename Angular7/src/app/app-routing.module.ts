import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BirimGosterComponent } from './birim-goster/birim-goster.component';
import { BirimSilComponent } from './birim-sil/birim-sil.component';
import { BirimGuncelleComponent } from './birim-guncelle/birim-guncelle.component';
import { BirimEkleComponent } from './birim-ekle/birim-ekle.component';
import { UpdateFormComponent } from './update-form/update-form.component';
import { CreateUnitComponent } from './create-unit/create-unit.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { RouteguardService } from './services/routeguard.service';

const routes: Routes = [
  {path: 'unit', component: BirimGosterComponent , canActivate: [RouteguardService]},
  {path: '', redirectTo: 'unit', pathMatch: 'full' , canActivate: [RouteguardService]},
  {path: 'unitDelete', component: BirimSilComponent , canActivate: [RouteguardService]},
  {path: 'login', component: LoginComponent  },
  {path: 'logout', component: LogoutComponent},
  {path: 'register', component: RegisterComponent  },
  {path: 'unitUpdate', component: BirimGuncelleComponent , canActivate: [RouteguardService] },
  {path: 'createUnit', component: CreateUnitComponent , canActivate: [RouteguardService] },
  {path: 'updateUnit', component: UpdateFormComponent, canActivate: [RouteguardService] },
  {path: 'unitCreate', component: BirimEkleComponent, canActivate: [RouteguardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
