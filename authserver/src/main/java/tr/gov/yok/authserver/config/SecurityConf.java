package tr.gov.yok.authserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.savedrequest.NullRequestCache;
import tr.gov.yok.authserver.security.CustomUserDetailsService;
import tr.gov.yok.authserver.security.JwtAuthenticationEntryPoint;
import tr.gov.yok.authserver.security.JwtTokenProvider;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true,prePostEnabled = true)
public class SecurityConf extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;
	@Autowired
	CustomUserDetailsService customUserDetailsService;
	@Autowired
	JwtTokenProvider jwtTokenProvider;
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
	}	
	   @Override
	    protected void configure(HttpSecurity http) throws Exception {

	      http.csrf().disable();
	        http.cors().disable();
	        http.exceptionHandling().authenticationEntryPoint(unauthorizedHandler);
	        http.requestCache().requestCache(new NullRequestCache());
	        http.httpBasic();
	        http.apply(new JwtConfigurer(jwtTokenProvider));
	        http.authorizeRequests().antMatchers("/", "/favicon.ico", "/**/*.png", "/**/*.gif", "/**/*.svg", "/**/*.jpg", "/**/*.html", "/**/*.css", "/**/*.js").permitAll();
	        http.authorizeRequests()
	                .antMatchers("/signin/**").permitAll()
	                .anyRequest().authenticated();

	        http.exceptionHandling().accessDeniedPage("/login");

	    }
	    @Bean
	    public PasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	    }

		@Bean(BeanIds.AUTHENTICATION_MANAGER)
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

	    @Override
	    public void configure(WebSecurity web) throws Exception {

	        web.ignoring()
	                .antMatchers("/eureka/**");

	    }

}
