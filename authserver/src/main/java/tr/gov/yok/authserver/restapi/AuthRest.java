package tr.gov.yok.authserver.restapi;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import tr.gov.yok.authserver.security.JwtTokenProvider;

@RestController
@CrossOrigin(origins = "*")
public class AuthRest {

	@Autowired
	JwtTokenProvider tokenProvider;
	
	
	@GetMapping("/getauth")
	public ResponseEntity<?> getAuth(){
		
		return  ResponseEntity.ok().body(SecurityContextHolder.getContext().getAuthentication());
	}
	
	@GetMapping("/getauth2")
	public UserDetails getAuth2(){
		
		return   (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
}
