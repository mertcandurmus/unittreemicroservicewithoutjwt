package tr.gov.yok.unitserver.restapi;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tr.gov.yok.unitserver.config.jwtTokenValidation;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*")
public class AuthRest {

	@Autowired
	jwtTokenValidation tokenProvider;
	
	@GetMapping("/authx")
	public ResponseEntity<?> getAuth( HttpServletRequest req, HttpServletResponse response){
		  HttpSession session = req.getSession();
		  String id = session.getId();
		  System.out.println("inside localhost:8081/auth/authx");
		  System.out.println(id);
			String jwt = tokenProvider.resolveToken(req);
			System.out.println(jwt);
			String url ="http://localhost:8080/getauth";
			
		//	UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
			//        .queryParam("token", jwt);
		//	System.out.println(builder);
		  RestTemplate restTemplate = new RestTemplate();
	      HttpHeaders headers =  new  HttpHeaders();
	      headers.set("Authorization", "Bearer " + jwt);
	      headers.add("Cookie", "JSESSIONID=" + id); 
	      HttpEntity entity =  new  HttpEntity(headers);
	      ResponseEntity<?> responseEntity = 
	    		  restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	      System.out.println(responseEntity.getBody()); 
	      
	      String str   =  (String) responseEntity.getBody();
	      String[] arrOfStr =  str.split(",");
	
	      System.out.println(arrOfStr[0]); 
	      
		return responseEntity;
	}
	
	@GetMapping("/authx2")
	public ResponseEntity<?> getAuth2( HttpServletRequest req, HttpServletResponse response){
		
		  HttpSession session = req.getSession();
		  String id = session.getId();
		  System.out.println("inside localhost:8081/auth/authx2");
		  System.out.println(id);
			String jwt = tokenProvider.resolveToken(req);
			System.out.println(jwt);
			String url ="http://localhost:8080/getauth2";

		  RestTemplate restTemplate = new RestTemplate();
	      HttpHeaders headers =  new  HttpHeaders();
	      headers.set("Authorization", "Bearer " + jwt);
	  
	      HttpEntity entity =  new  HttpEntity(headers);
	      ResponseEntity<?> responseEntity = 
	    		  restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	      String str   =  (String) responseEntity.getBody();
	   
	      
	      ObjectMapper obj=new ObjectMapper();
	      
	      try {
			User usr= obj.readValue(str, User.class);
			System.out.println("----------**----");
			System.out.println(usr);
			System.out.println("----------**----");
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      String []splitterString=str.split("\"");
	      String role = splitterString[13];
//	      for (int i=0; i<splitterString.length; i++) 
//	      { 
//	    	  System.out.println(splitterString[i]);
//	         
//	      }
//	      System.out.println(responseEntity.getBody()); 
//	      System.out.println(splitterString[13]);

	      
	  //    UserDetails user  = (UserDetails) responseEntity;
	   // System.out.println(user);
		return responseEntity;
	}
}
