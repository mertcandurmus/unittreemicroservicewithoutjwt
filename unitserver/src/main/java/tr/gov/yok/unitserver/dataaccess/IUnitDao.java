package tr.gov.yok.unitserver.dataaccess;

import java.util.List;

import tr.gov.yok.unitserver.entities.Unit;
import tr.gov.yok.unitserver.entities.UnitType;

public interface IUnitDao {

	List<Unit> getAll();

	void add(Unit unit);

	void delete(Unit unit);

	void update(Unit unit);

	Unit getById(int id);

	List<Unit> getAttechedUnit(int attachedunit);

	List<Unit> getByType(String type);

	String getType(int type_id);

	String getType2(int id);

	List<UnitType> getAllType();

}
