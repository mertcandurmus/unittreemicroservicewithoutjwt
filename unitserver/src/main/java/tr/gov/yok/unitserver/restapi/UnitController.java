package tr.gov.yok.unitserver.restapi;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tr.gov.yok.unitserver.business.IUnitService;
import tr.gov.yok.unitserver.config.jwtTokenValidation;
import tr.gov.yok.unitserver.entities.Unit;
import tr.gov.yok.unitserver.entities.UnitType;

@RestController
@RequestMapping("/unit")
@CrossOrigin(origins = "*")
public class UnitController {

	public IUnitService iUnitService;
	
	@Autowired
	jwtTokenValidation tokenProvider;

	@Autowired
	public UnitController(IUnitService iUnitService) {

		this.iUnitService = iUnitService;
	}

	@GetMapping("/units")
	@PreAuthorize("hasAnyAuthority('ROLE_USER','ROLE_ADMIN')")
	public List<Unit> get() {

		return this.iUnitService.getAll();
	}

	@GetMapping("/types")
	@PreAuthorize("hasAnyAuthority('ROLE_USER','ROLE_ADMIN')")
	public List<UnitType> getTypes( HttpServletRequest req) {

		System.out.println(SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString());
		System.out.println(SecurityContextHolder.getContext().getAuthentication().isAuthenticated());
		String jwt = tokenProvider.resolveToken(req);
		System.out.println(tokenProvider.getRoles(jwt));
		
		return this.iUnitService.getAllType();
	}

	@PostMapping("/add")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public void add(@RequestBody Unit unitone) {
		this.iUnitService.add(unitone);

	}

	@PostMapping("/update")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')") 
	public void update(@RequestBody Unit unitone) {
		this.iUnitService.update(unitone);
	System.out.println(SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString());
	}

	@PostMapping("/delete")
	@PreAuthorize("hasAnyAuthority('ROLE_USER','ROLE_ADMIN')")
	public void delete(@RequestBody Unit unitone) {
		this.iUnitService.delete(unitone);
	}

	@PostMapping("/delete/{id}")
	@PreAuthorize("hasAnyAuthority('ROLE_USER','ROLE_ADMIN')")
	public void deleteById(@PathVariable int id) {
		Unit unit = this.iUnitService.getById(id);
		this.iUnitService.delete(unit);
	}

	@GetMapping("/units/{id}")
	@PreAuthorize("hasAnyAuthority('ROLE_USER','ROLE_ADMIN')")
	public Unit getById(@PathVariable int id) {
		return this.iUnitService.getById(id);

	}

	@GetMapping("/unitAttach/{attachedunit}")
	@PreAuthorize("hasAnyAuthority('ROLE_USER','ROLE_ADMIN')")
	public List<Unit> getAttechedUnit(@PathVariable int attachedunit) {
		return this.iUnitService.getAttechedUnit(attachedunit);

	}

	@GetMapping("/unitType/{type}")
	@PreAuthorize("hasAnyAuthority('ROLE_USER','ROLE_ADMIN')")
	public List<Unit> getAttechedUnit2(@PathVariable String type) {
		return this.iUnitService.getByType(type);
	}
}
